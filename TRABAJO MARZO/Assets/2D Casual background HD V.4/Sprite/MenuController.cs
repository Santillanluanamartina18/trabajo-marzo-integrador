using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    // Nombre de la escena del juego
    public string UNITY2024MARZO;

    // M�todo para iniciar el juego
    public void StartGame()
    {
        // Carga la escena del juego cuando se presiona el bot�n "Start"
        SceneManager.LoadScene(0);
    }

    // M�todo para pausar el juego
    public void PauseGame()
    {
        // Pausa el tiempo en el juego
        Time.timeScale = 0f;
        // Aqu� podr�as desactivar elementos de la interfaz de usuario del juego para mostrar un men� de pausa, por ejemplo.
        Debug.Log("Juego pausado");
    }

    // M�todo para salir del juego
    public void ExitGame()
    {
        // Salir de la aplicaci�n (funciona solo en compilaci�n)
        Application.Quit();
        // Si est�s en el editor de Unity, puedes utilizar el modo editor para detener la ejecuci�n.
        Debug.Log("Juego finalizado");
    }
}



