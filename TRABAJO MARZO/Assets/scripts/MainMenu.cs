using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour
{

    public void STARTPLAY()
    {
        SceneManager.LoadScene("UNITY 2024 MARZO"); // Cargar el escenario del juego
    }

    public void PAUSE()
    {
        // Reanudar el tiempo del juego
        Time.timeScale = 1f;

        // Ocultar el men� de pausa
        // (aqu� podr�as desactivar el panel de pausa que mostraste antes)
    }

    // M�todo para salir del juego
    public void EXIT()
    {
        // Salir de la aplicaci�n (esto solo funciona en las compilaciones)
        Application.Quit();
    }
}





