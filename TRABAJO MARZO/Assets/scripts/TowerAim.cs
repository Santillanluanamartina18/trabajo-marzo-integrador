using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAim : MonoBehaviour
{
    [SerializeField]
    private GameObject _target;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = _target.transform.position - transform.position;
        Debug.DrawRay(transform.position, direction);

        Quaternion targetDirectionQuaternion = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetDirectionQuaternion, Time.deltaTime);
    }
}