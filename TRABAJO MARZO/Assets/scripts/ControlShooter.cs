
using UnityEngine;
using GeneralGame; // Importa el namespace de GameManager

public class ControlShooter : MonoBehaviour
{
    private GameManager gameManager;

    public GameObject balaPrefab;
    public Transform puntoDisparo;
    public float frecuenciaDeDisparo = 4;
    public int vidaShooter;

    private float tiempoUltimoDisparo;

    void Start()
    {
        gameManager = GameManager.Instance;
        if (gameManager == null)
        {
            Debug.LogError("No se encontr� el GameManager.");
        }
    }

    void Update()
    {
        if (gameManager != null && gameManager.Player != null)
        {
            GameObject player = gameManager.Player; // Acceder correctamente a la propiedad Player

            Vector3 direccionAlPlayer = (player.transform.position - puntoDisparo.position).normalized;
            Quaternion rotacionHaciaPlayer = Quaternion.LookRotation(direccionAlPlayer);

            transform.rotation = Quaternion.Slerp(transform.rotation, rotacionHaciaPlayer, Time.deltaTime);

            tiempoUltimoDisparo += Time.deltaTime;

            if (tiempoUltimoDisparo >= frecuenciaDeDisparo)
            {
                DispararBala(direccionAlPlayer);
                tiempoUltimoDisparo = 0f;
            }
        }
    }

    void DispararBala(Vector3 direccion)
    {
        if (balaPrefab != null && puntoDisparo != null)
        {
            GameObject bala = Instantiate(balaPrefab, puntoDisparo.position, Quaternion.identity);
            Rigidbody rb = bala.GetComponent<Rigidbody>();
            rb.velocity = direccion * 10f;
            Destroy(bala, 5);
        }
    }

    public void RecibirDano(int dano)
    {
        vidaShooter -= dano;
        if (vidaShooter <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            RecibirDano(5);
        }
    }
}





