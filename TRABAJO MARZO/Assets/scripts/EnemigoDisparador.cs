using UnityEngine;

public class EnemigoDisparador : MonoBehaviour
{
    public GameObject proyectilPrefab; // Prefab del proyectil
    public Transform puntoDeDisparo; // Punto de disparo del proyectil
    public float velocidadProyectil = 10f; // Velocidad del proyectil
    public float frecuenciaDisparo = 2f; // Frecuencia de disparo en segundos
    public float rangoDeteccion = 10f; // Rango de detecci�n del jugador

    private Transform jugador; // Referencia al transform del jugador
    private float tiempoUltimoDisparo; // Tiempo del �ltimo disparo

    private void Start()
    {
        // Obtener el transform del jugador
        jugador = GameObject.FindGameObjectWithTag("Player").transform;

        // Inicializar el tiempo del �ltimo disparo al inicio
        tiempoUltimoDisparo = Time.time;
    }

    private void Update()
    {
        // Comprobar si el jugador est� dentro del rango de detecci�n
        if (Vector3.Distance(transform.position, jugador.position) <= rangoDeteccion)
        {
            // Comprobar si ha pasado suficiente tiempo desde el �ltimo disparo
            if (Time.time - tiempoUltimoDisparo > frecuenciaDisparo)
            {
                // Disparar hacia el jugador
                Disparar();

                // Actualizar el tiempo del �ltimo disparo
                tiempoUltimoDisparo = Time.time;
            }
        }
    }

    private void Disparar()
    {
        // Calcular la direcci�n hacia el jugador
        Vector3 direccion = (jugador.position - puntoDeDisparo.position).normalized;

        // Crear una nueva instancia del proyectil desde el prefab y en la posici�n del punto de disparo
        GameObject nuevoProyectil = Instantiate(proyectilPrefab, puntoDeDisparo.position, Quaternion.identity);

        // Obtener el Rigidbody del proyectil y aplicarle velocidad en la direcci�n calculada
        Rigidbody rbProyectil = nuevoProyectil.GetComponent<Rigidbody>();
        if (rbProyectil != null)
        {
            rbProyectil.velocity = direccion * velocidadProyectil;
        }
    }
}


