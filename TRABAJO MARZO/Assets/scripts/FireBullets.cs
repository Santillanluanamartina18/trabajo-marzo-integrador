using System.Collections;
using UnityEngine;

public class FireBullets : MonoBehaviour
{
    [SerializeField]
    private GameObject _bullets;

    [SerializeField]
    private float _timer = 2f;
    private float _timerCount = 0f;

    [SerializeField]
    private int _counter;
    private int _maxCounter = 20;

    private void Start()
    {
        StartCoroutine(FireBulletsCoroutine());
    }

    private void Update()
    {
        _timerCount += Time.deltaTime;

        if (_timerCount > _timer)
        {
            FireBullet();
            _timerCount = 0f;
        }
    }

    private void FireBullet()
    {
        Instantiate(_bullets, transform.position, transform.rotation);
    }

    private IEnumerator FireBulletsCoroutine()
    {
        Debug.Log("Inicio coroutine");
        for (int i = 0; i < _maxCounter; i++)
        {
            _counter++;
            FireBullet();
            yield return new WaitForSeconds(_timer);
        }
        Debug.Log("Fin Coroutine");
    }
}




