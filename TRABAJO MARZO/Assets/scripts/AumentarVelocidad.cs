using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AumentarVelocidad : MonoBehaviour
{
    public float velocidadAumento = 2f; // Cu�nto se incrementar� la velocidad

    private float velocidadOriginal;
    public float velocidadMovimiento = 5f; // Velocidad original del jugador

    void Start()
    {
        // Guardamos la velocidad original del jugador
        velocidadOriginal = velocidadMovimiento;
    }

    void Update()
    {
        // Movimiento b�sico
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 movimiento = new Vector3(horizontal, 0, vertical) * velocidadMovimiento * Time.deltaTime;
        transform.Translate(movimiento, Space.World);
    }

    void OnCollisionEnter(Collision collision)
    {
        // Si el jugador colisiona con el objeto "Hola", aumenta la velocidad
        if (collision.gameObject.CompareTag("Hola"))
        {
            AumentarVelocidadJugador();
        }
    }

    // Funci�n para aumentar la velocidad del jugador
    void AumentarVelocidadJugador()
    {
        velocidadMovimiento *= velocidadAumento; // Aumenta la velocidad
        Debug.Log("La velocidad del jugador ha aumentado!");
    }
}
