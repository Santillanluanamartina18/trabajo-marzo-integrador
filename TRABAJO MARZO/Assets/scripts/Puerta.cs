using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public Transform puerta;
    public float SpeedPuerta = 1.0f;
    public Transform PuertaAbierta;
    public Transform PuertaCerrada;
    Vector3 Positions;
    public bool isUnlocked = false;
    public AudioClip soundClip; // Clip de sonido que se reproducir�
    private AudioSource audioSource; // Referencia al AudioSource

    void Start()
    {
        Positions = PuertaCerrada.position;
        audioSource = GetComponent<AudioSource>(); // Obtener el AudioSource
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }

        // Asignar el clip de sonido al AudioSource
        audioSource.clip = soundClip;
    }



    void Update()
    {
        if (isUnlocked && puerta.position != Positions)
        {
            puerta.position = Vector3.Lerp(puerta.position, Positions, Time.deltaTime * SpeedPuerta);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (isUnlocked)
            {
                Positions = PuertaAbierta.position;
            }

            else
            {
                Debug.Log("La puerta est� bloqueada. Debes encontrar la llave antes de abrirla.");
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Positions = PuertaCerrada.position;
        }
    }
}
