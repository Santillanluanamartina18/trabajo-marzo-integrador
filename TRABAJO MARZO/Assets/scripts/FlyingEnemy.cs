using UnityEngine;

public class FlyingEnemy : MonoBehaviour
{
    public float amplitude = 1f; // Amplitud del movimiento en el eje Y
    public float frequency = 1f; // Frecuencia del movimiento
    public float flightSpeed = 2f; // Velocidad de vuelo del enemigo
    private Vector3 startPosition; // Posici�n inicial del enemigo

    void Start()
    {
        startPosition = transform.position; // Guardar la posici�n inicial del enemigo
    }

    void Update()
    {
        // Calcula el desplazamiento en el eje Y utilizando la funci�n sinusoidal
        float yOffset = Mathf.Sin(Time.time * frequency) * amplitude;

        // Calcula la nueva posici�n del enemigo
        Vector3 newPosition = startPosition + Vector3.up * yOffset;

        // Calcula la direcci�n hacia la nueva posici�n
        Vector3 direction = (newPosition - transform.position).normalized;

        // Mueve al enemigo hacia la nueva posici�n con velocidad constante
        transform.Translate(direction * flightSpeed * Time.deltaTime);
    }
}
