using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform player; // Referencia al transform del jugador
    public float speed = 5f; // Velocidad de seguimiento

    void Update()
    {
        if (player != null)
        {
            // Calcula la direcci�n hacia el jugador
            Vector3 direction = player.position - transform.position;

            // Normaliza la direcci�n para que el movimiento sea suave
            direction.Normalize();

            // Calcula el movimiento basado en la direcci�n y la velocidad
            Vector3 movement = direction * speed * Time.deltaTime;

            // Aplica el movimiento al personaje seguidor
            transform.position += movement;
        }
    }
}
