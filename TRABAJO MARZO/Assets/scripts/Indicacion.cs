using UnityEngine;
using UnityEngine.UI;

public class Indicacion : MonoBehaviour
{
    public Text cartelIndicacion;

    private void Start()
    {
        // Ocultar el cartel de indicación al inicio
        OcultarCartelIndicacion();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Mostrar el cartel de indicación cuando el jugador se acerque
            MostrarCartelIndicacion();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Ocultar el cartel de indicación cuando el jugador se aleje
            OcultarCartelIndicacion();
        }
    }

    private void MostrarCartelIndicacion()
    {
        cartelIndicacion.gameObject.SetActive(true);
    }

    private void OcultarCartelIndicacion()
    {
        cartelIndicacion.gameObject.SetActive(false);
    }
}

