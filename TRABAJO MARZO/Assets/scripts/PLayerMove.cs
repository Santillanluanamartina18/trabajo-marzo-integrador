using UnityEngine;

public class PLayerMove : MonoBehaviour
{
    public float velocidadInicial = 10f; // Velocidad inicial del jugador
    public float velocidadActual; // Velocidad actual del jugador
    public float velocidadDisminuida = 5f; // Velocidad cuando es tocado por un enemigo
    public float tiempoDeRecuperacion = 2f; // Tiempo de recuperación de la velocidad normal

    private bool esTocado = false; // Variable para controlar si el jugador ha sido tocado por un enemigo

    void Start()
    {
        velocidadActual = velocidadInicial; // Inicializa la velocidad del jugador
    }

    void Update()
    {
        // Movimiento del jugador
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 movimiento = new Vector3(movimientoHorizontal, 0f, movimientoVertical) * velocidadActual * Time.deltaTime;
        transform.Translate(movimiento);
    }

    void OnCollisionEnter(Collision other)
    {
        // Si el jugador colisiona con un enemigo
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (!esTocado)
            {
                esTocado = true;
                velocidadActual = velocidadDisminuida; // Reduce la velocidad del jugador
                Invoke("RecuperarVelocidad", tiempoDeRecuperacion); // Programa la recuperación de la velocidad normal
            }
        }
    }

    void RecuperarVelocidad()
    {
        velocidadActual = velocidadInicial; // Recupera la velocidad normal del jugador
        esTocado = false; // Reinicia la bandera para poder ser tocado nuevamente
    }
}

