using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HacerMasGrande : MonoBehaviour
{
    public float factorCrecimiento = 1.5f; // Factor por el cual el jugador se hace m�s grande

    void OnCollisionEnter(Collision collision)
    {
        // Si el jugador colisiona con el objeto "Item", se hace m�s grande
        if (collision.gameObject.CompareTag("ITEM"))
        {
            HacerMasGrandeJugador();
        }
    }

    // Funci�n para hacer m�s grande al jugador
    void HacerMasGrandeJugador()
    {
        transform.localScale *= factorCrecimiento; // Aumenta la escala del jugador
        Debug.Log("El personaje se ha hecho m�s grande!");
    }
}
