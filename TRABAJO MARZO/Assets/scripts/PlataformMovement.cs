using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformMovement : MonoBehaviour
{
    public GameObject[] Cubos;

    public float plataformSpeed = 2;

    private int CubosIndex = 0;


    // Update is called once per frame
    void Update()
    {
        MovePlataform();
    }

    void MovePlataform()
    {
        if (Vector3.Distance(transform.position, Cubos[CubosIndex].transform.position) < 0.1f)
        {
            CubosIndex++;
            if (CubosIndex >= Cubos.Length)
            {
                CubosIndex = 0;
            }
        }

        transform.position = Vector3.MoveTowards(transform.position, Cubos[CubosIndex].transform.position, plataformSpeed * Time.deltaTime);
    }
}
