using UnityEngine;
using GeneralGame;  // Importar el namespace donde est� GameManager

public class Enemy : MonoBehaviour
{
    // M�todo que se llama cuando el enemigo colisiona con algo
    private void OnCollisionEnter(Collision collision)
    {
        // Verifica si el objeto con el que colisiona es el jugador
        if (collision.gameObject.CompareTag("Player"))
        {
            // Llama al m�todo en GameManager para que el jugador pierda vida
            GameManager.Instance.PerderVida();
        }
    }

    // M�todo que se llama cuando el enemigo muere
    public void OnDeath()
    {
        GameManager.Instance.PerderVida();
    }
}
