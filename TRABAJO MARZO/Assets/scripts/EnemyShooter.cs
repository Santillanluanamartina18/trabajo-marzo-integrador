using UnityEngine;

public class EnemyShooter : MonoBehaviour
{
    public GameObject bulletPrefab; // Prefab del proyectil
    public Transform firePoint; // Punto de origen del disparo
    public float fireRate = 1f; // Tasa de disparo en segundos
    public float bulletSpeed = 10f; // Velocidad del proyectil
    public int numberOfBullets = 3; // N�mero de balas a disparar
    private float nextFireTime; // Tiempo hasta el pr�ximo disparo

    void Update()
    {
        // Verificar si es el momento de disparar
        if (Time.time >= nextFireTime)
        {
            Shoot(); // Llamar a la funci�n de disparo
            nextFireTime = Time.time + 1f / fireRate; // Actualizar el tiempo del pr�ximo disparo
        }
    }

    void Shoot()
    {
        // Calcular el �ngulo entre las balas
        float angleStep = 360f / numberOfBullets;

        // Instanciar balas en diferentes direcciones
        for (int i = 0; i < numberOfBullets; i++)
        {
            float angle = i * angleStep;
            Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, rotation * firePoint.rotation);

            // Obtener el componente EnemyBullet del proyectil y ajustar su velocidad
            EnemyBullet bulletScript = bullet.GetComponent<EnemyBullet>();
            if (bulletScript != null)
            {
                bulletScript.speed = bulletSpeed;
            }
        }
    }
}


