using UnityEngine;
using UnityEngine.SceneManagement;

namespace GeneralGame
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }
        public HUD hud;
        public GameObject Player;  // Aqu� se almacena el jugador
        public int PuntosTotales { get; private set; }
        private int vidas = 3;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.LogError("Ya hay una instancia de GameManager presente.");
                Destroy(gameObject);
            }
        }

        public void SumarPuntos(int puntosSumar)
        {
            PuntosTotales += puntosSumar;
            if (hud != null)
            {
                hud.ActualizarPuntos(PuntosTotales);
            }
        }

        public void PerderVida()
        {
            vidas -= 1;
            if (vidas == 0)
            {
                SceneManager.LoadScene(1); // Reinicia el nivel si las vidas llegan a 0
            }
            if (hud != null)
            {
                hud.DesactivarVida(vidas); // Actualiza el HUD para reflejar las vidas restantes
            }
        }

        public bool RecuperarVida()
        {
            if (vidas == 3)
            {
                return false;
            }
            if (hud != null)
            {
                hud.ActivarVida(vidas); // Actualiza el HUD para activar la vida recuperada
            }
            vidas += 1;
            return true;
        }
    }
}















