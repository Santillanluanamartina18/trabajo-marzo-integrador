using UnityEngine;
using UnityEngine.UI;

public class ReclutamientoContador : MonoBehaviour
{
    private int contadorReclutas = 0;
    public Text textoContador;

    private void Start()
    {
        ActualizarTextoContador();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision Detected"); // A�ade este Debug.Log() para comprobar si la colisi�n se detecta correctamente
        if (other.CompareTag("Corazon"))
        {
            IncrementarContador();
            Destroy(other.gameObject); // Destruye el objeto de coraz�n al ser reclutado
        }
    }

    private void IncrementarContador()
    {
        contadorReclutas++;
        ActualizarTextoContador();
    }

    private void ActualizarTextoContador()
    {
        if (textoContador != null)
        {
            textoContador.text = "Corazones reclutados: " + contadorReclutas.ToString();
        }
    }
}



