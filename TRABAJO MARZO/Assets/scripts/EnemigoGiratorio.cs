using UnityEngine;

public class EnemigoGiratorio : MonoBehaviour
{
    public Transform centroDeGiro; // Punto alrededor del cual girar� el enemigo
    public float velocidadRotacion = 30f; // Velocidad de rotaci�n del enemigo en grados por segundo

    void Update()
    {
        // Obtener la direcci�n desde el enemigo hacia el centro de giro
        Vector3 direccionAlCentro = centroDeGiro.position - transform.position;

        // Calcular la rotaci�n que el enemigo debe tener para mirar hacia el centro de giro
        Quaternion rotacionHaciaCentro = Quaternion.LookRotation(direccionAlCentro, Vector3.up);

        // Aplicar la rotaci�n gradualmente
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotacionHaciaCentro, velocidadRotacion * Time.deltaTime);
    }
}

