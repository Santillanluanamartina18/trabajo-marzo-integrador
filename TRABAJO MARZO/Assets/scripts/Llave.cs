using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Llave : MonoBehaviour
{
    public Puerta PuertaPorAbrir;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PuertaPorAbrir.isUnlocked = true;
        }
        Destroy(gameObject);
    }
}
