using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public Transform startPoint; // Punto de inicio del movimiento
    public Transform endPoint; // Punto final del movimiento
    public float speed = 2f; // Velocidad de movimiento del enemigo
    private float t = 0f; // Parámetro de interpolación

    void Update()
    {
        // Incrementar el parámetro de interpolación basado en la velocidad y el tiempo transcurrido
        t += speed * Time.deltaTime;

        // Calcular la nueva posición interpolada entre los puntos de inicio y fin
        Vector3 newPosition = Vector3.Lerp(startPoint.position, endPoint.position, t);

        // Asignar la nueva posición al enemigo
        transform.position = newPosition;

        // Si el parámetro de interpolación supera 1, cambiar la dirección de movimiento
        if (t >= 1f)
        {
            // Intercambiar los puntos de inicio y fin
            Transform temp = startPoint;
            startPoint = endPoint;
            endPoint = temp;

            // Reiniciar el parámetro de interpolación
            t = 0f;
        }
    }
}

