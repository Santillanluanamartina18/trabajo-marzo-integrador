using UnityEngine;

public class DisparadorDeBalas : MonoBehaviour
{
    public GameObject prefabBala;
    public Transform puntoDisparo;
    public float velocidadBala = 10f;

    void Disparar()
    {
        // Encuentra el jugador por su tag
        GameObject player = GameObject.FindWithTag("Player");

        // Verifica si se encontr� el jugador
        if (player != null)
        {
            // Calcula la direcci�n hacia el jugador
            Vector3 direccionAlJugador = (player.transform.position - puntoDisparo.position).normalized;

            // Instancia la bala
            GameObject bala = Instantiate(prefabBala, puntoDisparo.position, Quaternion.identity);

            // Obtiene el Rigidbody de la bala
            Rigidbody rb = bala.GetComponent<Rigidbody>();

            // Asigna la velocidad constante en la direcci�n del jugador
            if (rb != null)
            {
                rb.velocity = direccionAlJugador * velocidadBala;
            }
        }
        else
        {
            Debug.LogError("No se encontr� al jugador en la escena. Aseg�rate de asignar el tag 'Player' al objeto del jugador.");
        }
    }
}

