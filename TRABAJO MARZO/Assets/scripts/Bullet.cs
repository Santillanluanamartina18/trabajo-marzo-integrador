using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody MyRb;
    public float Speed;


    private void Start()
    {
        MyRb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        MyRb.velocity = new Vector2(+Speed, 0);
        Destroy(gameObject, 30f);
    }
}