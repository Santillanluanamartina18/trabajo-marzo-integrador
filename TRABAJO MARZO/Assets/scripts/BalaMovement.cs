using UnityEngine;

public class BalaMovement : MonoBehaviour
{
    public float velocidad = 10f;
    public int dano = 25;

    void Update()
    {
        // Mover la bala en la direcci�n hacia adelante
        transform.Translate(Vector3.forward * velocidad * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Verificar si la colisi�n es con un enemigo
        if (collision.gameObject.CompareTag("Player"))
        {
            // Obtener el componente ControlShooter del objeto enemigo y causarle da�o
            ControlShooter controlShooter = collision.gameObject.GetComponent<ControlShooter>();
            if (controlShooter != null)
            {
                controlShooter.RecibirDano(dano);
            }
        }

        // Destruir la bala
        Destroy(gameObject);
    }
}

