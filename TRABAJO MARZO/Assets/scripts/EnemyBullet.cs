using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public int damage = 10; // Cantidad de da�o causada por el proyectil
    public float speed = 10f; // Velocidad del proyectil
    private Transform target; // Referencia al transform del objetivo (el jugador)

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform; // Busca y asigna el transform del jugador
    }

    void Update()
    {
        if (target != null)
        {
            // Calcula la direcci�n hacia el jugador
            Vector2 direction = (target.position - transform.position).normalized;

            // Mueve el proyectil hacia el jugador
            transform.Translate(direction * speed * Time.deltaTime);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
            if (playerHealth != null)
            {
                playerHealth.TakeDamage(damage); // Llama al m�todo TakeDamage en el script del jugador
                Destroy(gameObject); // Destruye el proyectil despu�s de impactar con el jugador
            }
        }
    }
}

