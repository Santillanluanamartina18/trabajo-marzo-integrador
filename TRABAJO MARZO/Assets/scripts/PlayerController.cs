using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float rapidezDesplazamiento = 15.0f;
    public Rigidbody Rb;
    public float fuerzaSalto = 10f;
    private float dirX;
    private float GirarPlayer;
    public int maxSaltos = 2; // Esto determina mi doble salto
    private int saltosRealizados = 0;
    private bool puedeSaltar = true;
    public GameObject lava2;
    private int vidaJugador = 3; // Variable para controlar la vida del jugador

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetButtonDown("Jump") && (saltosRealizados < maxSaltos || puedeSaltar))
        {
            Saltar();
        }

        // Si la vida llega a 0, cambia a la escena de "Game Over"
        if (vidaJugador <= 0)
        {
            SceneManager.LoadScene("game over");  // Cambia a la escena "GameOver" cuando la vida es 0
        }

        // Si se presiona la tecla R, vuelve a la escena "UNITY MARZO 2024"
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("UNITY MARZO 2024");  // Cambia a la escena "UNITY MARZO 2024"
        }
    }

    void Saltar()
    {
        if (saltosRealizados < maxSaltos)
        {
            Rb.velocity = new Vector3(Rb.velocity.x, 0, Rb.velocity.z);
            Rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
            saltosRealizados++;

            if (saltosRealizados == 1)
            {
                puedeSaltar = true;
            }
        }
    }

    // Este m�todo se llama cuando el jugador entra en contacto con un objeto
    public void OnCollisionEnter(Collision collision)
    {
        // Si el jugador entra en contacto con el objeto "lava2", cambia a la escena "UNITY MARZO 2024"
        if (collision.gameObject.CompareTag("lava2"))
        {
            SceneManager.LoadScene("UNITY 2024 MARZO");  // Cambia a la escena "UNITY MARZO 2024"
        }

        // Si el jugador colisiona con el objeto "Start", cambia a la escena "GameOver"
        if (collision.gameObject.CompareTag("Start"))  // Verifica si colisiona con el objeto Start
        {
            SceneManager.LoadScene("game over");  // Cambia a la escena "GameOver"
        }
    }

    // M�todo para reducir la vida del jugador (puedes llamarlo desde otras partes del juego)
    public void ReducirVida(int cantidad)
    {
        vidaJugador -= cantidad;
        if (vidaJugador < 0)
        {
            vidaJugador = 0;
        }
    }
}
