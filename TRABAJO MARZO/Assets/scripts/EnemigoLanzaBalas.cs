using UnityEngine;

public class EnemigoLanzaBalas : MonoBehaviour
{
    public GameObject balaPrefab; // Prefab de la bala
    public Transform puntoDeLanzamiento; // Punto de lanzamiento de la bala
    public float fuerzaLanzamiento = 20f; // Fuerza con la que se lanzar� la bala
    public float frecuenciaLanzamiento = 0.5f; // Frecuencia de lanzamiento en segundos
    public int cantidadBalasPorDisparo = 3; // Cantidad de balas por disparo

    private float tiempoUltimoLanzamiento; // Tiempo del �ltimo lanzamiento

    private void Start()
    {
        // Inicializar el tiempo del �ltimo lanzamiento al inicio
        tiempoUltimoLanzamiento = Time.time;
    }

    private void Update()
    {
        // Comprobar si ha pasado suficiente tiempo desde el �ltimo lanzamiento
        if (Time.time - tiempoUltimoLanzamiento > frecuenciaLanzamiento)
        {
            // Lanzar varias balas
            for (int i = 0; i < cantidadBalasPorDisparo; i++)
            {
                LanzarBala();
            }

            // Actualizar el tiempo del �ltimo lanzamiento
            tiempoUltimoLanzamiento = Time.time;
        }
    }

    private void LanzarBala()
    {
        // Crear una nueva bala desde el prefab y en la posici�n del punto de lanzamiento
        GameObject nuevaBala = Instantiate(balaPrefab, puntoDeLanzamiento.position, puntoDeLanzamiento.rotation);

        // Obtener el Rigidbody de la bala y aplicarle fuerza hacia adelante
        Rigidbody rbBala = nuevaBala.GetComponent<Rigidbody>();
        if (rbBala != null)
        {
            rbBala.AddForce(puntoDeLanzamiento.forward * fuerzaLanzamiento, ForceMode.Impulse);
        }
    }
}
