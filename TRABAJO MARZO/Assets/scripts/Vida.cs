using UnityEngine;  // Necesario para MonoBehaviour
using GeneralGame;  // Aseg�rate de importar el namespace correcto

public class Vida : MonoBehaviour
{
    public void PerderVida()
    {
        GameManager.Instance.PerderVida();
    }

    public void RecuperarVida()
    {
        GameManager.Instance.RecuperarVida();
    }
}
