using UnityEngine;

public class Torreta : MonoBehaviour
{
    public Transform puntoDisparo;
    public GameObject proyectilPrefab;
    public float velocidadDisparo = 10.0f;
    public float frecuenciaDisparo = 0.5f;

    private float tiempoUltimoDisparo = 0.0f;

    void Update()
    {
        if (Time.time - tiempoUltimoDisparo >= 1.0f / frecuenciaDisparo)
        {
            Disparar();
            tiempoUltimoDisparo = Time.time;
        }
    }

    void Disparar()
    {
        if (proyectilPrefab != null && puntoDisparo != null)
        {
            // Calcula una rotaci�n aleatoria alrededor del eje Y
            Quaternion rotacionAleatoria = Quaternion.Euler(0, Random.Range(0, 360), 0);

            GameObject proyectil = Instantiate(proyectilPrefab, puntoDisparo.position, rotacionAleatoria);
            Rigidbody proyectilRigidbody = proyectil.GetComponent<Rigidbody>();
            if (proyectilRigidbody != null)
            {
                // Usa la direcci�n hacia adelante del proyectil para determinar la velocidad
                proyectilRigidbody.velocity = proyectil.transform.forward * velocidadDisparo;
            }
        }
    }
}

