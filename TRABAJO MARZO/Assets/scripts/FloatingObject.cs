using UnityEngine;

public class FloatingObject : MonoBehaviour
{
    public float floatSpeed = 1f; // Velocidad de flotaci�n
    public float floatHeight = 0.5f; // Altura de flotaci�n

    private Vector3 startPos; // Posici�n inicial

    void Start()
    {
        startPos = transform.position; // Guardar la posici�n inicial del objeto
    }

    void Update()
    {
        // Calcular el desplazamiento vertical usando la funci�n seno
        float yOffset = Mathf.Sin(Time.time * floatSpeed) * floatHeight;

        // Actualizar la posici�n del objeto sumando el desplazamiento vertical al eje Y
        transform.position = startPos + new Vector3(0f, yOffset, 0f);
    }
}

