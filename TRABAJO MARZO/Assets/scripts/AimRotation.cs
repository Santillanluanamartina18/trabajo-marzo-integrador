using UnityEngine;

public class AimRotation : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private GameObject balaPrefab;
    [SerializeField] private Transform puntoDisparo;
    public float frecuenciaDisparo = 10f;
    private float tiempoUltimoDisparo;

    void Update()
    {
        if (target == null)
        {
            Debug.LogWarning("No se ha asignado un objetivo al script AimRotation en el objeto " + gameObject.name);
            return;
        }

        Vector3 targetDirection = target.position - puntoDisparo.position;
        Debug.DrawRay(puntoDisparo.position, targetDirection, Color.magenta);

        if (targetDirection != Vector3.zero)
        {
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime);

            // Actualizar el tiempo de disparo continuamente
            tiempoUltimoDisparo += Time.deltaTime;

            // Disparar balas hacia el jugador con la frecuencia especificada
            if (tiempoUltimoDisparo >= frecuenciaDisparo)
            {
                DispararBala();
                tiempoUltimoDisparo = 0f; // Reiniciar el tiempo de disparo
            }
        }
    }

    void DispararBala()
    {
        if (balaPrefab != null && puntoDisparo != null)
        {
            GameObject bala = Instantiate(balaPrefab, puntoDisparo.position, Quaternion.identity);
            // Configura la direcci�n de la bala hacia el objetivo
            bala.GetComponent<Rigidbody>().velocity = (target.position - puntoDisparo.position).normalized * 10f;
            // Aqu� podr�as configurar la velocidad, da�o o cualquier otra caracter�stica de la bala
        }
    }
}

