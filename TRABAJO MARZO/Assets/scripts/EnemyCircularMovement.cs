using UnityEngine;

public class EnemyCircularMovement : MonoBehaviour
{
    public Transform centerPoint; // Punto central del c�rculo
    public float radius = 5f; // Radio del c�rculo
    public float speed = 2f; // Velocidad de movimiento del enemigo
    private float angle = 0f; // �ngulo actual en radianes

    void Update()
    {
        // Incrementar el �ngulo basado en la velocidad y el tiempo transcurrido
        angle += speed * Time.deltaTime;

        // Calcular las coordenadas X e Y en el c�rculo alrededor del centroPoint
        float x = centerPoint.position.x + Mathf.Cos(angle) * radius;
        float y = centerPoint.position.y + Mathf.Sin(angle) * radius;

        // Asignar la nueva posici�n al enemigo
        transform.position = new Vector3(x, y, transform.position.z);
    }
}
